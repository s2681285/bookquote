package nl.utwente.di.bookQuote;

import java.util.HashMap;

public class Quoter {

    private HashMap<String, Double> map;

    public Quoter() {
        // Initialize the book prices HashMap
        map = new HashMap<>();
        map.put("1", 10.0);
        map.put("2", 45.0);
        map.put("3", 20.0);
        map.put("4", 35.0);
        map.put("5", 50.0);
    }

    public double getBookPrice(String isbn) {
        return map.getOrDefault(isbn, 0.0);
    }
}
